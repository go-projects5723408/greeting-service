# Build stage
FROM golang:latest AS build-stage

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the go.mod and go.sum files first and download the dependencies
# This is done separately to leverage Docker cache
COPY go.mod go.sum ./
RUN go mod download

# Copy everything from the current directory to the PWD (Present Working Directory) inside the container
COPY . .

# Build the application
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main .

# Runtime stage
FROM alpine:latest

WORKDIR /app

# Copy the compiled binary from the build stage
COPY --from=build-stage /app/main .

# This container exposes port 8080 to the outside world
EXPOSE 8080

# Command to run the executable
ENTRYPOINT ["/app/main"]

