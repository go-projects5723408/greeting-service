package routers

import (
	"github.com/clim018/greeting-service/handlers"
	"github.com/labstack/echo/v4"
)


func RegisterRoutes(e *echo.Echo) {

    // Routing
    e.GET("/", handlers.Hello)
}
