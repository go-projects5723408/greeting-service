package db

import (
	"context"
	"log"

	"github.com/clim018/greeting-service/utils"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
)

func ConnectDB() *pgxpool.Pool {
	conn, err := pgxpool.Connect(context.Background(), utils.GetDBConnectionString())
	if err != nil {
		log.Fatal("Unable to connect to database: ", zap.Error(err))
	}
	return conn
}
