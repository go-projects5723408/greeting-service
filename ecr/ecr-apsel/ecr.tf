# Create ECR Repository
resource "aws_ecr_repository" "greeting-service" {
  name                 = "greeting-service"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}