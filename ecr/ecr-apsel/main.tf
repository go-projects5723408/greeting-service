# Setup terraform providers
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.8.0"
    }
  }

  required_version = ">= 1.1.7"
}

# Setup terraform cloud and workspace
terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "clim"

    workspaces {
        name = "ecr-us-west-1"
    }
  }

  required_version = ">= 1.1.7"
}

# Setup AWS provider
provider "aws" {
  region = var.aws_region
}