package utils

import (
	"fmt"
	"github.com/spf13/viper"
)

func LoadConfigs() error {
	viper.SetConfigFile("./configs/config.yaml") 
	viper.SetConfigType("yaml")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()

	if err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}
	
	return err
}

func GetDBConnectionString() string {
	user := viper.GetString("database.user")
	password := viper.GetString("database.password")
	host := viper.GetString("database.host")
	dbname := viper.GetString("database.dbname")

	return fmt.Sprintf("user=%s password=%s host=%s dbname=%s", user, password, host, dbname)
}
