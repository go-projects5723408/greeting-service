package models

type Greeting struct {
    ID string `db:"id"`
    Message string `db:"message"`
}
