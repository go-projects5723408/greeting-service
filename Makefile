# Makefile

.PHONY: dependencies

all: dependencies build run

build: 
	go build -o main .

run: 
	./main

dependencies:
	go get -u github.com/labstack/echo/v4
	go get -u github.com/labstack/echo-contrib
	go get -u github.com/spf13/viper
	go get -u github.com/jackc/pgx/v4/pgxpool
	go get -u go.uber.org/zap