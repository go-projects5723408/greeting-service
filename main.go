package main

import (
	"log/slog"

	"github.com/clim018/greeting-service/routers"
	"github.com/clim018/greeting-service/utils"
	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
)

func init() {
	utils.LoadConfigs()
}

func main() {

	port := ":" + viper.GetString("server.port")
	e := echo.New()

	routers.RegisterRoutes(e)

    // db.ConnectDB()
	slog.Info("We are running fake DB")

    e.Logger.Fatal(e.Start(port))
}