# Greeting Service
This is a simple golang rest api that uses Echo, PostgresSQL and Dockerfile.

### Installation
```
make dependencies
```

### Running the Service
```
make run

or

air
```

### Add Gitlab's ENV in the project settings
```
AWS_ACCESS_KEY_ID='YOUR_ACCESS_KEY'
AWS_SECRET_ACCESS_KEY='YOUR_SECRET_KEY'
AWS_DEFAULT_REGION='us-west-1'
DOCKER_HOST='tcp://docker:2375'
ECR_REGISTRY='your-aws-id.dkr.ecr.us-west-1.amazonaws.com'
ECR_REPOSITORY='your-aws-id.dkr.ecr.us-west-1.amazonaws.com/your-repository'
```